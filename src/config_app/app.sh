#!/bin/sh

ldconfig # idk why here?

# local pc
local_ip=$(hostname -i)

# ip public
if [ -z "$remote_ip" ]; then
  remote_ip="2.0.0.100"
fi

# mysql
if [ -z "$ip_mysql" ]; then
  ip_mysql="127.0.0.1"
fi
if [ -z "$port_mysql" ]; then
  port_mysql="3306"
fi

# redis
if [ -z "$ip_redis" ]; then
  ip_redis="127.0.0.1"
fi
if [ -z "$port_redis" ]; then
  port_redis="6379"
fi

# password & user datebase
if [ -z "$pass_db" ]; then
  pass_db="GenshinImpactOffline2022"
fi
if [ -z "$user_db" ]; then
  user_db="work"
fi

#ip_redis="127.0.0.1"
#port_redis="6379"

#ip_mysql="127.0.0.1"
#port_mysql="3306"

echo "REPLACE_IT_TO_YOUR_DEVICE_IP=$local_ip"
echo "REPLACE_IT_TO_YOUR_ACCESS_IP=$remote_ip"

echo "REPLACE_IT_TO_YOUR_IP_MYSQL=$ip_mysql"
echo "REPLACE_IT_TO_YOUR_PORT_MYSQL=$port_mysql"

echo "REPLACE_IT_TO_YOUR_IP_REDIS=$ip_redis"
echo "REPLACE_IT_TO_YOUR_PORT_REDIS=$port_redis"

echo "REPLACE_IT_TO_YOUR_PASSWORD_DB=$pass_db"
echo "REPLACE_IT_TO_YOUR_USER_DB=$user_db"

find . -name "*.xml"  -exec sed -i "s/REPLACE_IT_TO_YOUR_PASSWORD_DB/$pass_db/g" {} +
find . -name "*.xml"  -exec sed -i "s/REPLACE_IT_TO_YOUR_USER_DB/$user_db/g" {} +

find . -name "*.xml"  -exec sed -i "s/REPLACE_IT_TO_YOUR_IP_MYSQL/$ip_mysql/g" {} +
find . -name "*.xml"  -exec sed -i "s/REPLACE_IT_TO_YOUR_PORT_MYSQL/$port_mysql/g" {} +

find . -name "*.xml"  -exec sed -i "s/REPLACE_IT_TO_YOUR_IP_REDIS/$ip_redis/g" {} +
find . -name "*.xml"  -exec sed -i "s/REPLACE_IT_TO_YOUR_PORT_REDIS/$port_redis/g" {} +

find . -name "*.xml"  -exec sed -i "s/REPLACE_IT_TO_YOUR_DEVICE_IP/$local_ip/g" {} +
find . -name "*.xml"  -exec sed -i "s/REPLACE_IT_TO_YOUR_ACCESS_IP/$remote_ip/g" {} +
find . -name "config.json"  -exec sed -i "s/REPLACE_IT_TO_YOUR_ACCESS_IP/$remote_ip/g" {} +

echo "Done! start server!!!"

supervisord