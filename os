FROM ubuntu:focal

RUN export DEBIAN_FRONTEND=noninteractive &&\
    dpkg-reconfigure dash &&\
    # install
    apt update && apt-get upgrade -y && apt-get -y install \ 
    # Python 2
    python2-minimal python2.7-dev \
    # Autostart
    supervisor

WORKDIR /root

# Add Lib GS
RUN echo '/root/server/lib' > /etc/ld.so.conf.d/genshin.conf

# Copy Server config
COPY src/config_dump/ ./server

# Copy docker file
COPY src/config_app/supervisord.conf ./supervisord.conf
COPY src/config_app/app.sh           ./app.sh

ENTRYPOINT ["sh", "app.sh"]