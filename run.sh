#!/bin/bash

VERSION_GAME="3.2"
NAME_FILE="gsserver"
USE_VER_TO="$NAME_FILE:$VERSION_GAME"
USE_VER="$USE_VER_TO-nodb"

metode=$1

if [ "$metode" = "b" ];then
 docker build -t "siakbary/$USE_VER_TO-nodb" --progress=plain -f os .;
fi

if [ "$metode" = "b_with_db" ];then
 docker build -t "siakbary/$USE_VER_TO-db" --progress=plain -f os_with_db_and_sdk .;
fi
if [ "$metode" = "p_with_db" ];then
 docker push siakbary/$USE_VER_TO-db
fi

if [ "$metode" = "localhost" ];then
echo "hello"
 # TODO
fi

if [ "$metode" = "up_db" ];then
docker compose up 
fi
# 3306 mysql | 2888 sdk server aka gc web server for login | 20001 ip gate server | 20041 game server | 20011 API GM
if [ "$metode" = "t" ];then
 docker run --rm -it \
 --env remote_ip="2.0.0.100" \
 --env ip_mysql="2.0.0.100" \
 --env port_mysql="8331" \
 --env ip_redis="2.0.0.100" \
 --env port_redis="8332" \
 -p 20011:20011 \
 -p 8383:20001 \
 -p 20041:20041/udp \
 -v //$(PWD)/src/bin/dbgate:/root/server/dbgate/dbgate:ro \
 -v //$(PWD)/src/bin/dispatch:/root/server/dispatch/dispatch:ro \
 -v //$(PWD)/src/bin/gameserver:/root/server/gameserver/gameserver:ro \
 -v //$(PWD)/src/bin/gateserver:/root/server/gateserver/gateserver:ro \
 -v //$(PWD)/src/bin/muipserver:/root/server/muipserver/muipserver:ro \
 -v //$(PWD)/src/bin/multiserver:/root/server/multiserver/multiserver:ro \
 -v //$(PWD)/src/bin/nodeserver:/root/server/nodeserver/nodeserver:ro \
 -v //$(PWD)/src/bin/oaserver:/root/server/oaserver/oaserver:ro \
 -v //$(PWD)/src/bin/pathfindingserver:/root/server/pathfindingserver/pathfindingserver:ro \
 -v //$(PWD)/src/bin/tothemoonserver:/root/server/tothemoonserver/tothemoonserver:ro \
 -v //$(PWD)/src/data_dump:/root/server/data:ro \
 siakbary/$USE_VER
fi

if [ "$metode" = "run_db_mysql" ];then
 docker run --rm -it \
 -p 3308:3306 \
 -v //$(PWD)/db_raw:/var/lib/mysql \
 -e MYSQL_ROOT_PASSWORD=f2c340a9-bf06-4345-9654-00b074b92fe8 \
 mysql:5.7-debian
fi

# Push Private
if [ "$metode" = "private_push" ];then
 docker push repo.yuuki.me/$USE_VER
fi

# Push Public
if [ "$metode" = "push" ];then
 docker push siakbary/$USE_VER
fi